package termproject;

import java.util.*;

/**
 * Title:        Term Project 2-4 Trees
 * Description:
 * Copyright:    Copyright (c) 2017
 * Company:
 * @author Micah Switzer and Zach Kemp
 * @version 1.0
 */
public class TwoFourTree
        implements Dictionary {

    private Comparator treeComp;
    private int size = 0;
    private TFNode treeRoot = null;

    public TwoFourTree(Comparator comp) {
        treeComp = comp;
    }
    
    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return (size == 0);
    }

    /**
     * Searches dictionary to determine if key is present
     * @param key to be searched for
     * @return object corresponding to key; null if not found
     */
    public Object findElement(Object key) {
        Item foundItem = null;
        TFNode currentNode = root();
        // prime the loop by looking up the first number
        int index = findFirstGreaterThanOrEqualTo(currentNode, key);
        // iterate and find the item
        while (true) {
            // only check the item if there's an item to check
            if (index != currentNode.getNumItems()) {
                Item tempItem = currentNode.getItem(index);
                if (treeComp.isEqual(key, tempItem.key())) {
                    // save the node if it matches
                    foundItem = tempItem;
                }
            }
            if (foundItem == null) {
                TFNode nextNode = currentNode.getChild(index);
                if (nextNode == null) {
                    // we've hit the end so break here
                    return null;
                }
                currentNode = nextNode; // continue iterating
                // look up the next index
                index = findFirstGreaterThanOrEqualTo(currentNode, key);
            }
            else {
                // we've found the item so break out of the loop
                return foundItem.element();
            }
        }
    }
    
    /**
     * Searches dictionary to determine if key is present, then
     * removes and returns corresponding object
     * @param key of data to be removed
     * @return object corresponding to key
     * @exception ElementNotFoundException if the key is not in dictionary
     */
    public Object removeElement(Object key)
            throws ElementNotFoundException {
        Item foundItem = null;
        TFNode currentNode = root();
        // prime the loop by looking up the first number
        int index = findFirstGreaterThanOrEqualTo(currentNode, key);
        // iterate and find the item
        while (true) {
            // only check the item if there's an item to check
            if (index != currentNode.getNumItems()) {
                Item tempItem = currentNode.getItem(index);
                if (treeComp.isEqual(key, tempItem.key())) {
                    // save the node if it matches
                    foundItem = tempItem;
                }
            }
            if (foundItem == null) {
                TFNode nextNode = currentNode.getChild(index);
                if (nextNode == null) {
                    // we've hit the end so break here
                    break;
                }
                currentNode = nextNode; // continue iterating
                // look up the next index
                index = findFirstGreaterThanOrEqualTo(currentNode, key);
            }
            else {
                // we've found the item so break out of the loop
                break;
            }
        }
        
        if (foundItem == null) {
            // throw an exception if we couldn't find a node
            throw new ElementNotFoundException();
        }
        
        if (currentNode.getChild(0) != null) { // this is not a leaf node
            // get the in order successor
            TFNode successor = getSuccessor(currentNode, index);
            // replace the item in the found node with the successor item
            currentNode.replaceItem(index, successor.getItem(0));
            // set the current node to the successor so that it 
            //  will be checked for underflow
            currentNode = successor;
            index = 0;
        }
        
        // remove the old item
        currentNode.removeItem(index);
        
        // this will always run on a leaf node
        checkUnderflow(currentNode);
        
        size--;
        return foundItem.element();
    }
    
    private TFNode root() {
        return treeRoot;
    }

    private void setRoot(TFNode root) {
        treeRoot = root;
    }

    private int findFirstGreaterThanOrEqualTo(TFNode t, Object key){
        int i = 0;
        if (t == null){
            return i;
        }
        for (i = 0; i < t.getNumItems(); i++) {
            // iterate to find the index
            if (treeComp.isLessThanOrEqualTo(key, t.getItem(i).key())){
                return i;
            }
        }
        return i;
    }
    
    private int whatChildIsThis(TFNode empty){
        TFNode par = empty.getParent();
        // iterate to find which index this child is at
        for (int i = 0; i <= par.getNumItems(); i++) {
            if (par.getChild(i) == empty){
                return i;
            }
        }
        return -1; //should not happen
    }

    /**
     * Inserts provided element into the Dictionary
     * @param key of object to be inserted
     * @param element to be inserted
     */
    public void insertElement(Object key, Object element) {
        // create a new item object
        Item newItem = new Item(key, element);
        
        // create a new node for the root
        if (isEmpty()) {
            TFNode newRoot = new TFNode();
            newRoot.addItem(0, newItem);
            setRoot(newRoot);
            size = 1;
            return;
        }
        
        int index = 0;
        TFNode currentNode = root();
        // iterate and find where it should be inserted
        index = findFirstGreaterThanOrEqualTo(currentNode, key);
        while(currentNode.getChild(index) != null) {
            currentNode = currentNode.getChild(index);
            index = findFirstGreaterThanOrEqualTo(currentNode, key);
        }
        // insert it at that spot
        currentNode.insertItem(index, newItem);
        // increase the size
        size++;
        // check for overfow
        checkOverflow(currentNode);
    }
    
    private void checkOverflow(TFNode currentNode){
        if(currentNode.getNumItems() > currentNode.getMaxItems()){
            TFNode parent;
            
            boolean newRoot = currentNode == root();
            if (newRoot){
                parent = new TFNode();
                setRoot(parent);
                currentNode.setParent(parent);
            }
            else{
                parent = currentNode.getParent();
            }
            
            // move item up
            Item moveUpItem = currentNode.getItem(1);
            int parentIndex =
                 findFirstGreaterThanOrEqualTo(parent, moveUpItem.key());
            parent.insertItem(parentIndex, moveUpItem);
            
            // create new left node
            Item leftItem = currentNode.getItem(0);
            TFNode leftChildNode = new TFNode();
            leftChildNode.setParent(parent);
            leftChildNode.addItem(0, leftItem);
            
            // move children if there are any
            TFNode leftChild = currentNode.getChild(0);
            TFNode rightChild = currentNode.getChild(1);
            if (leftChild != null && rightChild != null) {
                leftChild.setParent(leftChildNode);
                rightChild.setParent(leftChildNode);
                leftChildNode.setChild(0, leftChild);
                leftChildNode.setChild(1, rightChild);
            }
            
            // delete items and children as well
            currentNode.removeItem(1);
            currentNode.removeItem(0);
            
            // set the parent of the new child node
            parent.setChild(parentIndex, leftChildNode);
            
            // if we've created a new root,
            //  set the current node as the root's child
            if (newRoot) {
                parent.setChild(1, currentNode);
            }
            checkOverflow(parent);
        }
    }
    
    private void checkUnderflow(TFNode currentNode)
            throws TwoFourTreeException {
        // check if there's underflow
        if (currentNode.getNumItems() > 0) {
            return; // continue on our merry way if there isn't
        }
        if (currentNode == root()) {
            // if the root node is underflowed,
            //  set the new root to its child
            TFNode newRoot = currentNode.getChild(0);
            // set the new root
            setRoot(newRoot);
            if (newRoot != null) {
                // set the parent to null if it's not null itself
                newRoot.setParent(null);
            }
            // no more underflow
            return;
        }
        if (leftTransfer(currentNode)) {
            //System.out.println("did a left transfer");
            return;
        }
        if (rightTransfer(currentNode)) {
            //System.out.println("did a right transfer");
            return;
        }
        if (leftFusion(currentNode)) {
            //System.out.println("did a left fusion");
            return;
        }
        if (rightFusion(currentNode)) {
            //System.out.println("did a right fusion");
            return;
        }
        
        // error because the underflow wasn't fixed
        throw new TwoFourTreeException("Couldn't resolve underflow");
    }
    
    private boolean leftTransfer(TFNode node) {
        // get some info on the node
        int childIndex = whatChildIsThis(node);
        if (childIndex == 0) {
            // there is no left child so return false
            return false;
        }
        TFNode parent = node.getParent();
        TFNode leftSibling = parent.getChild(childIndex - 1);
        if (leftSibling.getNumItems() == 1) {
            // the left child doesn't have enough items
            return false;
        }
        
        // get the left sibling's rightmost child
        TFNode sibblingChild =
                leftSibling.getChild(leftSibling.getNumItems());
        
        // add the parent's item to the node
        node.addItem(0, parent.getItem(childIndex - 1));
        // replace the parent's item with
        //  the left sibling's rightmost item
        parent.replaceItem(childIndex - 1,
                leftSibling.getItem(leftSibling.getNumItems() - 1));
        // remove the old item from the left sibling
        leftSibling.setChild(leftSibling.getNumItems(), null);
        leftSibling.deleteItem(leftSibling.getNumItems() - 1);
        
        
        // transfer child node if it exists
        if (sibblingChild != null) {
            node.setChild(1, node.getChild(0));
            sibblingChild.setParent(node);
            node.setChild(0, sibblingChild);
        }
        
        // successfully completed a transfer
        return true;
    }
    
    private boolean rightTransfer(TFNode node) {
        // get info on the node
        int childIndex = whatChildIsThis(node);
        TFNode parent = node.getParent();
        if (childIndex == parent.getNumItems()) {
            // there is no right child so return false
            return false;
        }
        TFNode rightSibling = parent.getChild(childIndex + 1);
        if (rightSibling.getNumItems() == 1) {
            // the right child doesn't have enough items
            return false;
        }
        // get the sibbling's first child
        TFNode sibblingChild = rightSibling.getChild(0);
        // move the parent's item to the current node
        node.addItem(0, parent.getItem(childIndex));
        // replace the parent's item with the right sibbling's item
        parent.replaceItem(childIndex, rightSibling.getItem(0));
        // remove the old item from the right sibbling
        rightSibling.removeItem(0);
        
        // transfer child node if it exists
        if (sibblingChild != null) {
            sibblingChild.setParent(node);
            node.setChild(node.getNumItems(), sibblingChild);
        }

        // successfully completed a transfer
        return true;
    }
    
    private boolean leftFusion(TFNode node) {
        // get info on the node to see if we can do a left fusion
        int childIndex = whatChildIsThis(node);
        if (childIndex == 0) {
            // there is no left child so return false
            return false;
        }
        TFNode parent = node.getParent();
        // get the sibbling node
        TFNode leftSibling = parent.getChild(childIndex - 1);
        // move the item from the parent to the sibbling
        leftSibling.insertItem(1, parent.getItem(childIndex - 1));
        
        // try to get the current node's child
        TFNode nodeChild = node.getChild(0);
        if(nodeChild != null){
            // if it exists, move it to it's parent's sibbling
            nodeChild.setParent(leftSibling);
            leftSibling.setChild(leftSibling.getNumItems(), nodeChild);
        }
        // remove the old item
        parent.setChild(childIndex, leftSibling);
        parent.removeItem(childIndex - 1);
        
        // we might have caused underflow in the parent so check
        checkUnderflow(parent);
        return true;
    }
    
    private boolean rightFusion(TFNode node) {
        // get info on the node to see if we can do a right fusion
        int childIndex = whatChildIsThis(node);
        TFNode parent = node.getParent();
        if (childIndex == parent.getNumItems()) {
            // there is no right child so return false
            return false;
        }
        // get the sibbling node
        TFNode rightSibling = parent.getChild(childIndex + 1);
        // move the item from the parent to the sibbling
        rightSibling.insertItem(0, parent.getItem(childIndex));
        
        // try to get the current node's child
        TFNode nodeChild = node.getChild(0);
        if (nodeChild != null) {
            // if it exists, move it to it's parent's sibbling
            nodeChild.setParent(rightSibling);
            rightSibling.setChild(0, nodeChild);
        }
        // remove the old child
        parent.removeItem(childIndex);
        
        // we might have caused underflow in the parent so check
        checkUnderflow(parent);
        return true;
    }

    private TFNode getSuccessor(TFNode node, int idx) {
        // get the right node to start
        TFNode current = node.getChild(idx + 1); 
        while (current.getChild(0) != null) {
            // then keep looking at the leftmost one until
            //  we get to a leaf
            current = current.getChild(0);
        }
        return current;
    }
    
    public void printAllElements() {
        System.out.println("----------------------");
        int indent = 0;
        if (root() == null) {
            System.out.println("The tree is empty");
        }
        else {
            printTree(root(), indent);
        }
    }

    public void printTree(TFNode start, int indent) {
        if (start == null) {
            return;
        }
        for (int i = 0; i < indent; i++) {
            System.out.print(" ");
        }
        printTFNode(start);
        indent += 4;
        int numChildren = start.getNumItems() + 1;
        for (int i = 0; i < numChildren; i++) {
            printTree(start.getChild(i), indent);
        }
    }

    public void printTFNode(TFNode node) {
        int numItems = node.getNumItems();
        for (int i = 0; i < numItems; i++) {
            System.out.print(((Item) node.getItem(i)).element() + " ");
        }
        System.out.println();
    }

    // checks if tree is properly hooked up,
    //  i.e., children point to parents
    public void checkTree() {
        checkTreeFromNode(treeRoot);
    }

    private void checkTreeFromNode(TFNode start) {
        if (start == null) {
            return;
        }

        if (start.getParent() != null) {
            TFNode parent = start.getParent();
            int childIndex;
            for (childIndex = 0;
                    childIndex <= parent.getNumItems(); childIndex++) {
                if (parent.getChild(childIndex) == start) {
                    break;
                }
            }
            // if child wasn't found, print problem
            if (childIndex > parent.getNumItems()) {
                System.out.println("Child to parent confusion");
                printTFNode(start);
            }
        }

        if (start.getChild(0) != null) {
            for (int childIndex = 0;
                    childIndex <= start.getNumItems(); childIndex++) {
                if (start.getChild(childIndex) == null) {
                    System.out.println(
                            "Mixed null and non-null children");
                    printTFNode(start);
                }
                else {
                    if (start.getChild(childIndex).getParent() != start) {
                        System.out.println("Parent to child confusion");
                        printTFNode(start);
                    }
                    for (int i = childIndex - 1; i >= 0; i--) {
                        if (start.getChild(i) ==
                                start.getChild(childIndex)) {
                            System.out.println(
                                    "Duplicate children of node");
                            printTFNode(start);
                        }
                    }
                }

            }
        }

        int numChildren = start.getNumItems() + 1;
        for (int childIndex = 0; childIndex < numChildren; childIndex++) {
            checkTreeFromNode(start.getChild(childIndex));
        }

    }
    
    public static void main(String[] args) {
        Comparator myComp = new IntegerComparator();        
        TwoFourTree myTree = new TwoFourTree(myComp);
        
        // random test
        Queue<Object> randQueue = new LinkedList<Object>();
        Random random = new Random();
        final int RANDOM_TEST_SIZE = 10000;
        int rand = 0;
        for(int i = 0; i < RANDOM_TEST_SIZE; i++){
            rand = random.nextInt();
            myTree.insertElement(rand, rand);
            randQueue.add(rand);
        }
        for(int i = 0; i < RANDOM_TEST_SIZE; i++){
            Object val = randQueue.remove();
            Object removed = myTree.removeElement(val);
            if (!myComp.isEqual(val, removed)) {
                System.out.println("Mismatch!");
            }
        }
        if(myTree.isEmpty()){
            System.out.println("random test successful");
        }

        final int TEST_SIZE = 10000;
        for (int i = 0; i < TEST_SIZE; i++) {
            myTree.insertElement(new Integer(i), new Integer(i));
        }
        if ((int)myTree.findElement(TEST_SIZE/2) == TEST_SIZE/2) {
            System.out.println("find element succeeded");
        }
        if (myTree.findElement(TEST_SIZE) == null) {
            System.out.println("find element succeeded");
        }
        System.out.println("removing");
        for (int i = 0; i < TEST_SIZE; i++) {
            int out = (Integer) myTree.removeElement(new Integer(i));
            if (out != i) {
                throw new TwoFourTreeException(
                        "main: wrong element removed");
            }
        }
        
        System.out.println("done");
    }
}